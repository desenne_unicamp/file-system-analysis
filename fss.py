#!/usr/bin/env python3
"""
author: Carolina de Senne Garcia
RA 145656
c145656@dac.unicamp.br
desennecarol@gmail.com

Run this file:
python3 fss.py [filesystem] <hierarchy>

Description: https://lasca.ic.unicamp.br/paulo/cursos/so/2019s2/exp/exp03.html
Information from: https://www.nongnu.org/ext2-doc/ext2.html
"""
import sys
import time
import math
import queue

# Print file system sizes analysis
def printFileSizeAnalysis(number_files_sizes,total_files):
    def computePercentage(portion):
        p = (portion/total_files)*100
        # I chose to show more decimal places in order not to round the last percentages
        return "%.4f" % p
    print("+-----------+--------------------------+")
    print("|%-11s|%-26s|" % ("  Tamanho  ", "  Porcentagem cumulativa  "))
    print("+===========+==========================+")
    print("| %-10s| %-25s|" % ("1 B", computePercentage(number_files_sizes[0])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("2 B", computePercentage(number_files_sizes[1])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("4 B", computePercentage(number_files_sizes[2])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("8 B", computePercentage(number_files_sizes[3])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("16 B", computePercentage(number_files_sizes[4])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("32 B", computePercentage(number_files_sizes[5])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("64 B", computePercentage(number_files_sizes[6])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("128 B", computePercentage(number_files_sizes[7])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("256 B", computePercentage(number_files_sizes[8])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("512 B", computePercentage(number_files_sizes[9])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("1 KB", computePercentage(number_files_sizes[10])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("2 KB", computePercentage(number_files_sizes[11])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("4 KB", computePercentage(number_files_sizes[12])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("8 KB", computePercentage(number_files_sizes[13])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("16 KB", computePercentage(number_files_sizes[14])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("32 KB", computePercentage(number_files_sizes[15])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("64 KB", computePercentage(number_files_sizes[16])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("128 KB", computePercentage(number_files_sizes[17])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("256 KB", computePercentage(number_files_sizes[18])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("512 KB", computePercentage(number_files_sizes[19])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("1 MB", computePercentage(number_files_sizes[20])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("2 MB", computePercentage(number_files_sizes[21])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("4 MB", computePercentage(number_files_sizes[22])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("8 MB", computePercentage(number_files_sizes[23])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("16 MB", computePercentage(number_files_sizes[24])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("32 MB", computePercentage(number_files_sizes[25])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("64 MB", computePercentage(number_files_sizes[26])+" %"))
    print("+-----------+--------------------------+")
    print("| %-10s| %-25s|" % ("128 MB", computePercentage(number_files_sizes[27])+" %"))
    print("+-----------+--------------------------+")

# Get info from superblock (offset: 1024, lenght: 1024 bytes)
def getSuperblockInfo(f):
    f.seek(1024)
    # total number of inodes
    inodes_total = int.from_bytes(f.read(4),"little")
    # total nummber of blocks in filesystem
    fs_size_in_blocks = int.from_bytes(f.read(4), "little")
    # ignore number of reserved blocks
    f.read(4)
    # number of free blocks
    free_blocks_total = int.from_bytes(f.read(4), "little")
    # ignore number of free inodes and number of first block
    f.read(8)
    # read block size
    val = int.from_bytes(f.read(4), "little")
    block_size = 1024 * 2 ** val
    # ignore frament size
    f.read(4)
    # read number of blocks per group
    n_blocks_per_group = int.from_bytes(f.read(4), "little")
    # ignore number of fragments per groups
    f.read(4)
    # read number inodes per group
    n_inodes_per_group = int.from_bytes(f.read(4), "little")
    # compute total, free and used space in bytes
    return inodes_total, fs_size_in_blocks, free_blocks_total, block_size, n_blocks_per_group, n_inodes_per_group

# Get info from block group descriptor table (offset: block_size)
def getBlockGroupDescriptorTableInfo(f,block_size):
    # Get info from block group descriptor table (offset: block_size)
    block_id_bitmaps = []
    inode_id_bitmaps = []
    inode_tables = []
    for i in range(153): # there are 153 block groups
        f.seek(block_size+(i*32)) # each block group descriptor has lenght of 32 bytes
        bg_block_bitmap = int.from_bytes(f.read(4), "little")
        bg_inode_bitmap = int.from_bytes(f.read(4), "little")
        bg_inode_table = int.from_bytes(f.read(4), "little")
        block_id_bitmaps.append(bg_block_bitmap) # block ID for a block bitmap
        inode_id_bitmaps.append(bg_inode_bitmap) # block ID for an inode bitmap
        inode_tables.append(bg_inode_table)
    return block_id_bitmaps, inode_id_bitmaps, inode_tables

# Get info from bitmaps
def getBitmapsInfo(f,block_size,id_bitmaps,n_per_group):
    # report currently used/alocated blocks/inodes
    is_used = []
    for idbmp in id_bitmaps:
        f.seek(idbmp*block_size)
        for i in range (int(n_per_group/8)):
            bits = int.from_bytes(f.read(1), "little")
            for offset in range(8):
                bit = (bits >> offset) & 1
                if (bit==1):
                    is_used.append(True)
                else:
                    is_used.append(False)
    return is_used

# Compute number of contiguous free segments in the block list
def contiguousFree(block_is_used):
    free_segments = 0
    last_b = True
    for b in block_is_used:
        if b != last_b:
            if b == False:
                free_segments += 1
            last_b = b
    return free_segments

# read n blocks
def readNBlocks(f,n,block_count):
    blocks = []
    for i in range(n):
        if block_count <= 0:
            break
        b = int.from_bytes(f.read(4), "little") # read block ID
        blocks.append(b)
        block_count -= 1
    return block_count, blocks

# read indirect blocks and count number of discontinuities
def countContiguousIndirectBlocks(f,block_size, indirect_block, block_count):
    f.seek(indirect_block*block_size)
    return readNBlocks(f,int(block_size/4),block_count)

# read double/triple indirect blocks and count number of discontinuities
def countContiguousMultipleIndirectBlocks(f,block_size, multiple_indirect_block, block_count, depth):
    all_blocks = []
    for k in range(int(block_size/4)): # each block ID occupies 4 bytes
        if block_count <= 0:
            break
        f.seek(multiple_indirect_block*block_size+(k*4))
        indblock = int.from_bytes(f.read(4), "little") # read block ID
        if depth == "double":
            block_count, blocks = countContiguousIndirectBlocks(f,block_size,indblock,block_count)
            all_blocks += blocks
        if depth == "triple":
            block_count, blocks = countContiguousMultipleIndirectBlocks(f,block_size,indblock,block_count,"double")
            all_blocks += blocks
    return block_count, all_blocks

# Get info from inode, considering we are already on that inode (no seek needed)
def getInodeInfo(f,block_size):
    i_mode = int.from_bytes(f.read(2), "little")
    f.read(2)
    i_size = int.from_bytes(f.read(4), "little")
    f.read(32) # ignore a lot of stuff
    # read blocks in inode
    all_blocks = []
    block_count = int(math.ceil(i_size/block_size))
    # read direct blocks
    block_count, blocks = readNBlocks(f,12,block_count)
    all_blocks += blocks
    # read indirect block IDs
    block13 = int.from_bytes(f.read(4), "little")
    block14 = int.from_bytes(f.read(4), "little")
    block15 = int.from_bytes(f.read(4), "little")
    # read indirect-blocks
    block_count, blocks = countContiguousIndirectBlocks(f,block_size, block13, block_count)
    all_blocks += blocks
    # read double-indirect blocks
    block_count, blocks = countContiguousMultipleIndirectBlocks(f, block_size, block14, block_count, "double")
    all_blocks += blocks
    # read triple-indirect blocks
    block_count, blocks = countContiguousMultipleIndirectBlocks(f, block_size, block15, block_count, "triple")
    all_blocks += blocks
    return i_mode, i_size, all_blocks

# Get info from inode tables
def getInfoInodeTables(f,block_size,inode_tables,n_inodes_per_group):
    # array with number of files with size lower than 2^index bytes
    number_files_sizes = [0]*28
    total_files = 0
    total_contiguous_segments = 0
    # read each used inode
    for inidtb in inode_tables:
        for k in range(n_inodes_per_group):
            f.seek(inidtb*block_size+(k*128))
            # read inode
            i_mode, i_size, data_blocks = getInodeInfo(f,block_size)
            # count contiguous_segments (or discontinuities) for this inode
            contiguous_segments = 1
            for i in range(len(data_blocks)-1):
                prev_b = data_blocks[i]
                b = data_blocks[i+1]
                if b > prev_b+1:
                    contiguous_segments += 1
            total_contiguous_segments += contiguous_segments
            # regular file: consider in the size analysis (regular file = 0x8000)
            if ((i_mode >> 12) == 8):
                total_files += 1
                for i in range(28):
                    if i_size < 2**i:
                        number_files_sizes[i] += 1
    return number_files_sizes, total_files, total_contiguous_segments

# Analyze an entire filesystem
def analyzeFilesystem(filename):
    f = open(filename, 'rb')
    # read superblock
    inodes_total, fs_size_in_blocks, free_blocks_total, block_size, n_blocks_per_group, n_inodes_per_group = getSuperblockInfo(f)
    # compute free and used space in system
    total_space = fs_size_in_blocks*block_size
    free_space = free_blocks_total*block_size
    used_space = total_space - free_space
    # read block group descriptor table
    block_id_bitmaps, inode_id_bitmaps, inode_tables = getBlockGroupDescriptorTableInfo(f,block_size)
    # read currently alocated/free blocks
    block_is_used = getBitmapsInfo(f,block_size,block_id_bitmaps,n_blocks_per_group)
    # compute free segments
    free_segments = contiguousFree(block_is_used)
    # read currently used/free inodes
    inode_is_used = getBitmapsInfo(f,block_size,inode_id_bitmaps,n_inodes_per_group)
    # read inode tables
    number_files_sizes, total_files, inode_contiguous_segments = getInfoInodeTables(f,block_size,inode_tables,n_inodes_per_group)
    # print analysis
    print("1. No. de segmentos contíguos na lista de blocos livres: {0:d}".format(free_segments))
    print("2. Relação frag_livres: {:.2f} KB/segm".format((free_space/free_segments)/1000))
    print("3. Relação frag_ocupados: {:.2f} KB/segm".format((used_space/inode_contiguous_segments)/1000))
    print("4. Taxa de fragmentação dos i-nodes: {:.2f} no-segm/inode".format(inode_contiguous_segments/inodes_total))
    print("5. Distribuição de tamanhos de arquivos utilizando porcentagens cumulativas:\n")
    printFileSizeAnalysis(number_files_sizes,total_files)

# get the inode absolute address in the file, give an inode index
def getInodeAddress(inode_index,inode_tables,n_inodes_per_group,block_size):
    group = int((inode_index-1)/n_inodes_per_group)
    local_index = (inode_index-1) % n_inodes_per_group
    inode_add = inode_tables[group]*block_size+(local_index*128)
    return inode_add

# get the inodes and their types inside a given directory
def getInodesDir(f,blocks,block_size):
    inodes = []
    count = 0
    for b in blocks:
        f.seek(b*block_size)
        total_read = 0
        while total_read < block_size:
            inode = int.from_bytes(f.read(4), "little")
            if inode == 0:
                break
            rec_len = int.from_bytes(f.read(2), "little")
            total_read += rec_len
            f.read(1)
            file_type = int.from_bytes(f.read(1), "little")
            f.read(rec_len-8)
            if count > 1: # should never take first and second entries (this and parent directory)
                inodes.append((inode,file_type))
            count += 1
    return inodes

# get the inode for a give file/dir name (path_piece) inside a directory
def getInodePathPiece(f,blocks,block_size,path_piece):
    for b in blocks:
        f.seek(b*block_size)
        total_read = 0
        while total_read < block_size:
            inode = int.from_bytes(f.read(4), "little")
            if inode == 0:
                break
            rec_len = int.from_bytes(f.read(2), "little")
            total_read += rec_len
            name_len = int.from_bytes(f.read(1), "little")
            file_type = int.from_bytes(f.read(1), "little")
            name = f.read(name_len).decode() # read string from bytes
            if name==path_piece:
                return inode
            padding = rec_len-8-name_len
            f.read(padding)
    return -1

# find hierarchy base inode index
def findHierarchyBase(f,path,inode_tables,n_inodes_per_group,block_size):
    # root directory inode (2o in the inode table)
    inode_index = 2
    add = getInodeAddress(inode_index,inode_tables,n_inodes_per_group,block_size)
    f.seek(add)
    i_mode,_,all_blocks = getInodeInfo(f,block_size)
    for p in path:
        inode_index = getInodePathPiece(f,all_blocks,block_size,p)
        if inode_index == -1:
            print("Error: hierarchy not found.")
            exit(1)
        add = getInodeAddress(inode_index,inode_tables,n_inodes_per_group,block_size)
        f.seek(add)
        i_mode,_,all_blocks = getInodeInfo(f,block_size)
        if ((i_mode >> 12) != 4):
            print("Error: hierarchy must point to a directory.")
            exit(1)
    return inode_index

# get all inodes in a hierarchy from a base directory
def getAllInodes(f,inode_index,block_size,inode_tables,n_inodes_per_group):
    dir_inodes = queue.Queue()
    dir_inodes.put(inode_index)
    all_inodes = []
    while(dir_inodes.empty() != True):
        dir_inode = dir_inodes.get()
        all_inodes.append(dir_inode)
        add = getInodeAddress(dir_inode,inode_tables,n_inodes_per_group,block_size)
        f.seek(add)
        i_mode,i_size,data_blocks = getInodeInfo(f,block_size)
        # get subinodes and put them in the queue (if they are dirs) or in all_inodes list
        sub_inodes = getInodesDir(f,data_blocks,block_size)
        for (i_index,i_type) in sub_inodes:
            if i_type<2 or i_type>4: # unkown type, regular file, buffer file, socket file, symbolic link
                all_inodes.append(i_index)
            if i_type==2 and i_index!=dir_inode: # directory file
                dir_inodes.put(i_index)
    return all_inodes

# make an analysis on the given inodes
def inodesAnalysis(f,inodes,block_size,inode_tables,n_inodes_per_group):
    total_contiguous_segments = 0
    used_space = 0
    inodes_total = 0
    number_files_sizes = [0]*28
    total_files = 0
    for inode_index in inodes:
        add = getInodeAddress(inode_index,inode_tables,n_inodes_per_group,block_size)
        f.seek(add)
        i_mode,i_size,data_blocks = getInodeInfo(f,block_size)
        # count contiguous_segments (or discontinuities) for this inode
        contiguous_segments = 1
        for i in range(len(data_blocks)-1):
            prev_b = data_blocks[i]
            b = data_blocks[i+1]
            if b > prev_b+1:
                contiguous_segments += 1
        total_contiguous_segments += contiguous_segments
        # compute used space
        used_space += i_size
        # compute total number of inodes
        inodes_total += 1
        # compute file size analysis
        # if a regular files, consider in the size analysis
        if ((i_mode >> 12) == 8):
            total_files += 1
            for i in range(28):
                if i_size < 2**i:
                    number_files_sizes[i] += 1
    return used_space, total_contiguous_segments, inodes_total, number_files_sizes, total_files

# Analyze a hierarchy inside an entire filesystem
def analyzeHierarchy(filename,hierarchy):
    f = open(filename, 'rb')
    path = [s for s in hierarchy.split("/") if s!='']
    # get block size from superblock
    _,_,_,block_size,_,n_inodes_per_group = getSuperblockInfo(f)
    _,_,inode_tables = getBlockGroupDescriptorTableInfo(f,block_size)
    # find hierarchy base
    base_inode = findHierarchyBase(f,path,inode_tables,n_inodes_per_group,block_size)
    all_inodes = getAllInodes(f,base_inode,block_size,inode_tables,n_inodes_per_group)
    used_space, inode_contiguous_segments, inodes_total, number_files_sizes, total_files = inodesAnalysis(f,all_inodes,block_size,inode_tables,n_inodes_per_group)
    # print analysis
    print("3. Relação frag_ocupados: {:.2f} KB/segm".format((used_space/inode_contiguous_segments)/1000))
    print("4. Taxa de fragmentação dos i-nodes: {:.2f} no-segm/inode".format(inode_contiguous_segments/inodes_total))
    print("5. Distribuição de tamanhos de arquivos utilizando porcentagens cumulativas:\n")
    printFileSizeAnalysis(number_files_sizes,total_files)

def run():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print('Usage: fss.py <filename>')
        sys.exit(1)
    filename = sys.argv[1]
    if len(sys.argv) == 2:
        print('\nAnalizando sistema de arquivos: %s ...\n' % filename)
        analyzeFilesystem(filename)
    if len(sys.argv) == 3:
        hierarchy = sys.argv[2]
        print('\nAnalizando hierarquia: %s\ndo sistema de arquivos: %s ...\n' % (hierarchy,filename))
        analyzeHierarchy(filename,hierarchy)

if __name__ == '__main__':
    start_time = time.time()
    run()
    end_time = time.time()
    hours = int((end_time-start_time)/3600)
    minutes = int((end_time-start_time-(hours*3600))/60)
    seconds = int((end_time-start_time))-(hours*3600)-(minutes*60)
    print("\nElapsed Time: {0:d}h {1:d}m {2:d}s\n".format(hours,minutes,seconds))
